import Vue from 'vue'
import Router from 'vue-router'

// moved to pages from components
import Home from '@/pages/Home';
import Admin from '@/pages/Admin';
import Cart from '@/pages/Cart';

// Admin components
import AdminIndex from '@/pages/admin/Index';
import AdminCreate from '@/pages/admin/Create';
import AdminUpdate from '@/pages/admin/Update';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/admin',
      name: 'AdminIndex',
      component: AdminIndex,
      children: [        
      {
        path: '/create',
        name: 'AdminCreate',
        component: AdminCreate
      },
      {
        path: '/update/:id',
        name: 'AdminUpdate',
        component: AdminUpdate
      }
      ]
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    }
  ]
})
